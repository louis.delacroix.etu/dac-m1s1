terraform{
required_version = ">= 0.13.4"
	required_providers{
		openstack={
			source = "terraform-provider-openstack/openstack"
			version="~>1.35.0"
		}
	}
}

resource "openstack_compute_keypair_v2" "keypair" {
	provider=openstack
	name="ssh-key"
	public_key=file("../ssh/id_rsa.pub")
}

resource "openstack_compute_instance_v2""terraform_instances_ubuntu"{
	count=2
	name="terraform_instance-ubuntu-${count.index}"
	provider=openstack
	image_name="ubuntu-20.04"
	flavor_name="normale"
	key_pair=openstack_compute_keypair_v2.keypair.name
}
resource "openstack_compute_instance_v2""terraform_instances_centos"{
        count=2
        name="terraform_instance-centos-${count.index}"
        provider=openstack
        image_name="centos-7"
        flavor_name="normale"
        key_pair=openstack_compute_keypair_v2.keypair.name
}
