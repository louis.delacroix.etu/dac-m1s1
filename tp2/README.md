#Sujet  
Prise en main de l'IaC avec terraform

#Auteur  
Delacroix Louis

#Objectif:

Créer des instances dans openstack à l'aide de terraform

# Etape 1: Créer 4 instances Ubuntu

- On commence par télécharger OpenStack RC File V3 et on le place dans notre répertoire de travail
- On rédige un fichier de configuration terraform

> Dans notre cas old_config/main_1.tf

- On exécute la commande : source louis.delacroix.etu-openrc.sh
- On exécute la commande : terraform init
- On exécute la commande : terraform plan
- On exéxute la comannde : terraform apply

Si tout c'est bien passé les 4 instances identiques ont été créées

# Etape 2: Créer 2 instances Ubuntu et 2 instances Centos

> Même choses qu'au dessus sauf qu'il faut ajouter ajouter une règle au fichier de configuration qui créée deux instances centos
