# TP 1 : Prise en main d’Openstack

Dans ce TP nous allons utiliser l’instance Openstack mise en place par l’université de Lille. Cette instance se trouve à cette adresse : https://cloud.univ-lille.fr, vous devez avoir le VPN activé comme le réseau de la salle TIIR est isolé de celui de la FAC.

Pour faciliter notre prise en main, nous utiliserons les images déjà mise à disposition. Mais n'hésitez pas à vous documenter sur la création d’images dédié à openstack. Ces images dites "clef en main" vous permettent d’instancier et de vous connecter à la machine via SSH.

## Installation des instances
Le but premier de ce TP est de mettre en place de machines virtuelles qui vous serviront pendant l’ensemble de ce module et peut-être plus.

Vous devez disposer de trois ou quatre instances et d’y accéder via SSH en utilisant votre clé SSH.
Sécurisation des instances
Une fois vos machine instanciés, ouvrez le port de votre choix, et en utilisant le protocol de transport de votre choix, effectuez un bref échange entre poste de travail et vos instances. Effectuez la capture du trafic sur vos instances et votre poste de travail de manière simultanée. Expliquez le trafic que vous avez enregistré.

Sécurisez vos instances en utilisant le par-feux de votre choix. Aucunes connexions, sauf celles utiles à l’administration et la maintenance de vos instances doit être acceptées.

Effectuez de nouveau le processus de capture, expliquez.

Si vous avez le temps, prenez le temps de lire les articles de blog suivant :
https://www.cyberciti.biz/tips/linux-security.html
https://bash-prompt.net/guides/server-hacked/
https://www.redhat.com/sysadmin/security-monitoring-tripwire
https://bash-prompt.net/guides/lsof/

*Faites valider votre travail*

Tout mon travail a été retranscrit dans le fichier Rapport.txt
