## TP2 : Ansible

1. Vérifier l’installation d’Ansible sur votre poste de travail en utilisant le commande : `$ ansible --version`.

2. Créer le fichier hosts permettant de contacter les quatre serveurs que vous avez créés dans le TP précédent. Aidez-vous de la documentation officielle : [Ansible inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).

Le fichiers hosts pour contacter les 4 serveurs est rédigé, j'ai créé 2 groupes 1 pour les serveurs centos et l'autre pour les serveurs ubuntu.

3. Vérifiez que pouvez contacter les serveurs en utilisant Ansible, pour ce faire, vous pouvez vous servir de la commande ping intégrée à Ansible.

Pour contacter les serveurs avec Ansible j'ai utilisé la commande:

```sh
ansible -i hosts -m ping all
```

4. Créer un playbook Ansible, veuillez lire la documentation : [Ansible playbook](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html).

![playbook] (question4.png "playbook")


5. En utilisant votre playbook, installez docker et le firewall de votre choix.

Pour verifier son fonctionnement écrire 
```sh
ansible-playbook docker.yml firewall.yml -i hosts
```

6. Configurez votre firewall pour qu’il n’autorise pas les connexions uniquement sur les ports des services : HTTP, HTTP et SSH.

Voir fichier firewall.yml

7. Démarrer un serveur web sur le por HTTP en utilisant docker et ansible.
